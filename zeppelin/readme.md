Inspiré par :
- http://www.doanduyhai.com/blog/?p=2325
- https://medium.com/apache-zeppelin-stories/zeppelin-spark-cassandra-4f07fcaeef1a

Nous déployons zeppelin automatiquement avec Spark.

# Accéder à Zeppelin
Il convient dans un premier temps de récupérer l'adresse sur laquelle Zeppelin est exposé :
`kubectl describe services/my-spark-zeppelin`. Le champ `LoadBalancer Ingress` nous intéresse. 

Ne pas oublier d'ajouter le port (normalement 8080).


Vous pouvez accéder à Zeppelin directement depuis votre navigateur.

# Importation du Notebook
Importer le Notebook (Notebook > Import note) présent à l'URL suivante : `https://gitlab.com/sdtd/kubernetes/raw/master/zeppelin/RunningProcesses.json` (fichier `RunningProcesses.json`)

# Configuration de Zeppelin
Pour accéder à la configuration : Clic sur `anonymous` > `Interpreter`

Récupérer le mot de passe de Cassandra à l'aide de la commande `kubectl get secret --namespace default cassandra -o jsonpath="{.data.cassandra-password}" | base64 --decode`

Dans interpreter cassandra de zeppelin, changer la configuration :
```
cassandra.credentials.password 	<mot de passe Cassandra>
cassandra.credentials.username 	cassandra
cassandra.hosts 	cassandra 
```

Dans interpreter spark de zeppelin, changer la configuration :
```
spark.cassandra.auth.password 	<mot de passe Cassandra>
spark.cassandra.auth.username 	cassandra
spark.cassandra.connection.host 	cassandra 
```

Ajouter à la fin, une nouvelle dépendance à Spark (champ Artifact) :
```
 com.datastax.spark:spark-cassandra-connector_2.11:2.0.10 
```
