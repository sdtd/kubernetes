#!/bin/bash
if [ -z ${bucket_name} ] ; then
    echo "bucket_name variable not set... Aborting"
    exit 1
fi

export region="eu-west-3"

# We create the bucket
aws s3api create-bucket \
    --bucket ${bucket_name} \
    --create-bucket-configuration LocationConstraint=${region} \
    --region ${region}

# Bucket encryption
aws s3api put-bucket-encryption --bucket ${bucket_name} --server-side-encryption-configuration '{"Rules":[{"ApplyServerSideEncryptionByDefault":{"SSEAlgorithm":"AES256"}}]}'
