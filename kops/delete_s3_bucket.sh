#!/bin/bash

if [ -z ${bucket_name} ] ; then
    echo "bucket_name variable not set... Aborting"
    exit 1
fi

aws s3 rm s3://${bucket_name} --recursive

aws s3api delete-bucket --bucket ${bucket_name}