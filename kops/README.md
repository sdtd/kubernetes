# Deploy a Kubernetes cluster on AWS using Kops and Terraform
Follow this documentation to deploy a Kubernetes cluster on AWS, using Kops.

If you struggle somewhere, please create a Gitlab Issue, and assign me.
If you have some modifications to propose, all MR are welcome :)

## Prerequisites

Before digging into what is important, make sure that the following softwares are installed on your environment :
* [AWS CLI](https://docs.aws.amazon.com/fr_fr/cli/latest/userguide/installing.html)
* [Kubectl : Kubernetes CLI](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
* [Kops](https://github.com/kubernetes/kops#installing)

You will also need a functional AWS account with some credits on it. 

Moreover, when your AWS account is created, you will have to create an IAM user that have Administrator rights. To do so, please [follow this guide](https://docs.aws.amazon.com/IAM/latest/UserGuide/getting-started_create-admin-group.html).

Keep warm your __AccessKeyId__ and __SecretAccessKey__, you won't be able to recover them afterwards.
The best option is to store them within your aws-cli credentials file, located at `~/.aws/credentials`.

```
[default]
aws_access_key_id = <put yours>
aws_secret_access_key = <put yours>

[admin-profile]
aws_access_key_id = <put yours>
aws_secret_access_key = <put yours>
```

In this file, I've declared two profiles. The __default__ one will be used by Kops user to perform actions. The other one is the profile we've just created.

More info on profile management: https://docs.aws.amazon.com/cli/latest/userguide/cli-multiple-profiles.html

## Setup your AWS environment
Before deploying some machines, we will need an IAM user, dedicated to Kops, that can perform actions on the AWS account.
### Create the AWS IAM Kops user
For this part, you will need to use the __admin-profile__ that we defined on the `~/.aws/credentials` file. Read [here](https://docs.aws.amazon.com/cli/latest/userguide/cli-multiple-profiles.html) to know how to do it.

Once it's done, we will launch the script `create_aws_user.sh`. If will create the Kops IAM user with the correct rights.

You should record the SecretAccessKey and AccessKeyID in the returned JSON output, and then use them below:
```
# configure the aws client to use your new IAM user
aws configure           # Use your new access and secret key here
aws iam list-users      # you should see a list of all your IAM users here

# Because "aws configure" doesn't export these vars for kops to use, we export them now
export AWS_ACCESS_KEY_ID=$(aws configure get aws_access_key_id)
export AWS_SECRET_ACCESS_KEY=$(aws configure get aws_secret_access_key)
```

### Configure DNS
For Kops 1.6.2 or later, DNS is optional. In this example, we will use the ELB URL provided by AWS. If you want to use your own domain name, [follow this guide](https://github.com/kubernetes/kops/blob/master/docs/aws.md#configure-dns).

### Create the Cluster State Storage
#### Prepare local environment
We're ready to start creating our first cluster! Let's first set up a few environment variables to make this process easier.

In order to store the state and the representation of our cluster, we need to create a dedicated S3 bucket for `kops` to use. This bucket will become the source of truth for our cluster configuration.
The bucket name must be unique across the region, chose wisely...

For a gossip-based cluster (without domain name), make sure the name ends with k8s.local. For example:
```
export bucket_name=<your_s3bucket_name>
export NAME=myfirstcluster.k8s.local
export KOPS_STATE_STORE=s3://${bucket_name}
```
Run the above commands to set the environment, we will refer to these variables afterwards.

NOTICE : __Do not use__ capital letters for bucket_name

Once everything is ok, just run the script. `./create_s3_bucket.sh`.

## Create the cluster
We are now ready to initiate the cluster creation!

Script usage :
```bash
Usage: ./create_kops_cluster_local.sh [OPTIONS]

Utility tool to deploy a Kubernetes custer on AWS.
By default the cluster is deployed on one zone (eu-west-3a).
You can customize the deployment using the following arguments

Options:

-z, --zones         List of zones where to deploy the cluster (separated by a comma).               Default: eu-west-3a
-n, --node_size     Flavor of EC2 machine to use for nodes.                                         Default: t2.medium
-c, --node_count    Number of nodes to deploy.                                                      Default: 4
-m, --master_size   Flavor of EC2 machine to use for masters.                                       Default: t2.medium
-x, --master_count  Number of masters to deploy.                                                    Default: 1
-k, --no_kafka      Skip Kafka deployment                                                           Default: false
-s, --no_spark      Skip Spark deployment                                                           Default: false
-b, --no_cassandra  Skip Cassandra's deployment                                                     Default: false
-h, --help          Print this message
-v, --version       Print soft version
```

Follow the steps below if you want to deploy the cluster __manually__.

NOTICE : These steps have been run on a __Linux environment__, that's why you will only see bash commands. If you runs on Windows, try to use a tool like [Cmder](http://cmder.net/), and translate the commands that we will perform.
### Create the Kops cluster
#### Create the cluster configuration
We will need to note which availability zones are available to us. In this example we will be deploying our cluster to the `eu-west-3` region (Paris).

```
aws ec2 describe-availability-zones --region eu-west-3
```

The below command will generate a cluster configuration, but not start building it. Make sure that you have generated SSH key pair before creating the cluster.
NOTICE : This is just an example, that works, but we can improve HA as [described here](https://github.com/kubernetes/kops/blob/master/docs/high_availability.md#advanced-example).
```
kops create cluster \
  ${NAME} \
  --zones eu-west-3a \
  --node-size=t2.micro \
  --authorization=rbac \
  --cloud=aws \
  --node-count=2 \
  --master-size=t2.medium \
  --master-zones eu-west-3a \
  --master-count=1
```

You can customize whatever you want within this command, like the size of the masters or the nodes...
Once the command is executed, it will prepare the cluster and show you what needs to be done.

When you are sure that everything is ok, you can start the cluster deployment using :
```bash
kops update cluster ${NAME} --yes
```

The execution is quick, but the objects are not immediately created. You can follow the progress on your AWS console.

To ensure that cluster is working properly, use the following command :
```bash
kops validate cluster
```

## Ensure everything is ok
Remember when you installed `kubectl` earlier? The configuration for your cluster was automatically generated and written to `~/.kube/config` for you!

A simple Kubernetes API call can be used to check if the API is online and listening. Let's use `kubectl` to check the nodes.

``` bash
kubectl get nodes
```

You will see a list of nodes that should match the `--zones` flag defined earlier. This is a great sign that your Kubernetes cluster is online and working.

Also `kops` ships with a handy validation tool that can be ran to ensure your cluster is working as expected.
```bash
kops validate cluster
```

You can look at all the system components with the following command.

```bash
kubectl -n kube-system get po
```

## Deploy the Kubernetes dashboard
To have a better user experience, we will deploy a really useful Kubernetes AddOn named [Kubernetes dashboard](https://github.com/kubernetes/dashboard).

The good point is that we will deploy it on top on Kubernetes, so it will be really easy.

### Create the Administrator Service Account
To access the dashboard, we will need to authenticate as a ClusterAdmin. To do so, we have to create a [Service Account](https://kubernetes.io/docs/reference/access-authn-authz/service-accounts-admin/) and grant him ClusterAdmin rights.

The configuration is already prepared and is located under `deployments/dashboard/sa_role_binding.yml`, you can have a look at it if you want.

To create it on the Kubernetes cluster, execute the following command:
```bash
kubectl apply -f deployments/dashboard/sa_role_binding.yml
```

### Get the ServiceAccount token
As the ServiceAccount is not a _real_ user, it uses a token to authenticate. And we will use this token to authenticate to the Kubernetes Dashboard.
To get the token, simply execute the following command:

```
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')
```
You will see in the output a token. Keep it for the next steps.

### Deploy the dashboard
We are now ready to deploy the dashboard. Again, a list of Kubernetes objects that needs to be created have already been created. You just have to execute the following command:

```
 kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml
 ```

To view the state of the created objects, execute `kubectl -n kube-system get all`.

### Get the kubernetes admin credentials
In order to access the Kubernetes cluster endpoint, we have to provide some user credentials. The default user that we are going to use is the cluster-admin, created by default.
You can get the password using this command : 
```
kops get secrets kube --type secret -oplaintext
```
And login with the username `admin`.

### Navigate to the dashboard
We need to get the url where the Kubernetes is running on the internet. It actually corresponds to the AWS ELB DNS that has been created at cluster creation. We can retrieve the existing ELB using `kubectl`.

```
kubectl cluster-info
```
And copy the Cluster URL.

You can now access to the dashboard using the following URL: 
```
https://<cluster_URL>/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/
```
And then authenticate using the previously generated token.

__That's all!__ We have a running Kubernetes cluster and we successfully deployed an application over it.

## Spark deployment
Install spark : https://spark.apache.org/downloads.html

Deployment : https://spark.apache.org/docs/latest/running-on-kubernetes.html
```
./bin/spark-submit \
    --master k8s://https://<cluster master url> \
    --deploy-mode cluster \
    --name spark-pi \
    --class org.apache.spark.examples.SparkPi \
    --conf spark.executor.instance=5 \
    --conf spark.kubernetes.container.image=../spark/Dockerfile \
    local:///path/to/examples.jar
```

## Destroy the cluster

To destroy the entire cluster, run the following commands:

```
kops delete cluster ${NAME} --yes
```

## Enhancements
### High Availability
This cluster has been deployed on a single availability zone. Using only one master. This can be improved.

To have HA, we can deploy 3 masters across (at least) 2 regions, and also some nodes on the two regions.