#!/usr/bin/env bash
if [ -z ${bucket_name} ] ; then
    echo "bucket_name variable not set... Aborting"
    exit 1
fi

if [ -z ${NAME} ] ; then
    echo "NAME variable not set... Aborting"
    exit 1
fi

if [ -z ${KOPS_STATE_STORE} ] ; then
    echo "KOPS_STATE_STORE variable not set... Aborting"
    exit 1
fi

kops delete cluster ${NAME} --yes