#!/bin/bash
if [ -z ${access-key-id-to-delete} ] ; then
    echo "access-key-id-to-delete variable not set... Aborting"
    exit 1
fi

if [ -z ${admin-profile-name} ] ; then
    echo "admin-profile-name variable not set... Aborting"
    exit 1
fi

aws iam delete-access-key --user-name kops --access-key-id ${access-key-id-to-delete} --profile ${admin-profile-name}

aws iam remove-user-from-group --user-name kops --group-name kops --profile ${admin-profile-name}

aws iam detach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonEC2FullAccess --group-name kops
aws iam detach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonRoute53FullAccess --group-name kops
aws iam detach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonS3FullAccess --group-name kops
aws iam detach-group-policy --policy-arn arn:aws:iam::aws:policy/IAMFullAccess --group-name kops
aws iam detach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonVPCFullAccess --group-name kops

aws iam delete-user --user-name kops --profile ${admin-profile-name}

aws iam delete-group --group-name kops --profile ${admin-profile-name}
