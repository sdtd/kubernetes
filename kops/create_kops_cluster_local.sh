#!/usr/bin/env bash

if [ -z ${bucket_name} ] ; then
    echo "bucket_name variable not set... Aborting"
    exit 1
fi

if [ -z ${NAME} ] ; then
    echo "NAME variable not set... Aborting"
    exit 1
fi

if [ -z ${KOPS_STATE_STORE} ] ; then
    echo "KOPS_STATE_STORE variable not set... Aborting"
    exit 1
fi

#Default values, overriden by args
export zones="eu-west-3a"
export node_size="t2.medium"
export node_count=4
export master_size="t2.medium"
export master_count=1
export no_kafka=1
export no_spark=1
export no_cassandra=1

#Args parsing
GET_OPT=`getopt -o hvz:n:c:m:x:ksb --long help,version,zones:,node_size:,node_count:,master_size:,master_count:,no_kafka,no_spark,no_cassandra -n 'k8s automated Ensimag' -- "$@"`
VERSION="k8s SDTD 1 -- v1.0.0"
HELP_MESSAGE="Usage: $0 [OPTIONS]

Utility tool to deploy a Kubernetes custer on AWS.
By default the cluster is deployed on one zone (eu-west-3a).
You can customize the deployment using the following arguments

Options:

-z, --zones         List of zones where to deploy the cluster (separated by a comma).               Default: eu-west-3a
-n, --node_size     Flavor of EC2 machine to use for nodes.                                         Default: t2.medium
-c, --node_count    Number of nodes to deploy.                                                      Default: 4
-m, --master_size   Flavor of EC2 machine to use for masters.                                       Default: t2.medium
-x, --master_count  Number of masters to deploy.                                                    Default: 1
-k, --no_kafka      Skip Kafka deployment                                                           Default: false
-s, --no_spark      Skip Spark deployment                                                           Default: false
-b, --no_cassandra  Skip Cassandra's deployment                                                     Default: false
-h, --help          Print this message
-v, --version       Print soft version"

eval set -- "$GET_OPT"
while true
do
  case "${1}" in
    -h|--help)
      echo "$HELP_MESSAGE"; exit 0;;
    -v|--version)
      echo "$VERSION"; exit 0;;
    -z|--zones)
      zones=$2; shift 2;;
    -n|--node_size)
      node_size=$2; shift 2;;
    -c|--node_count)
      node_count=$2; shift 2;;
    -m|--master_size)
      master_size=$2; shift 2;;
    -x|--master_count)
      master_count=$2; shift 2;;
    -k|--no_kafka)
      no_kafka=0; shift 1;;
    -s|--no_spark)
      no_spark=0; shift 1;;
    -b|--no_cassandra)
      no_cassandra=0; shift 1;;
    --) shift; break;;
    *) echo "Options ${1} is not a known option."; echo "$HELP_MESSAGE"; exit 1;;
  esac
done

echo "# -------- Création du cluster.... -------- #"
kops create cluster \
  ${NAME} \
  --zones ${zones} \
  --authorization=rbac \
  --cloud=aws \
  --node-size=${node_size} \
  --node-count=${node_count} \
  --master-size=${master_size} \
  --master-zones ${zones} \
  --master-count=${master_count}

kops update cluster ${NAME} --yes

echo "# -------- Validation du cluster... -------- #"

while ! kops validate cluster | egrep -q "Your cluster .* is ready" 2>&1 > /dev/null; do
    clear
    kops validate cluster
    sleep 10
done
clear
kops validate cluster

if [[ "${NAME}" =~ .k8s.local$ ]] ; then
    export cluster_url=$(aws elb describe-load-balancers --region=eu-west-3 --query='LoadBalancerDescriptions[0].DNSName' --output=text)
else
    export cluster_url="api.${NAME}"
fi

if [[ "${cluster_url}" =~ ^None$ ]] ; then
    echo " ================>> COULD NOT SUCCESSFULLY RETRIEVE CLUSTER URL <<================ "
fi

echo "# -------- Déploiement du Dashboard.... -------- #"
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml

echo "# -------- Création du Service Account.... -------- #"
kubectl apply -f deployments/dashboard/sa_role_binding.yml

echo "# -------- Déploiement de Prometheus et Grafana... -------- #"
kubectl apply -f deployments/prometheus-grafana/prometheus-grafana.yml

echo "# -------- Initialisation de Helm au sein du cluster... -------- #"
helm init
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'

if [[ $no_kafka -eq 0 ]] ; then
    echo "# -------- Skipped Kafka deployment -------- #"
else
    echo "# -------- Déploiement de Kafka -------- #"

    kubectl apply -f deployments/kafka/zookeeper.yml
    kubectl apply -f deployments/kafka/kafka-service.yml

    echo "Little sleep let AWS bind an ELB to Kafka-Service...."
    sleep 5
    kafka_host_name=$(kubectl describe service kafka-service | grep LoadBalancer | grep Ingres | awk '{print $3}')
    kafka_deployment=$(echo "---
    kind: Deployment
    apiVersion: apps/v1
    metadata:
      name: kafka-broker0
    spec:
      selector:
        matchLabels:
          app: kafka
      template:
        metadata:
          labels:
            app: kafka
            id: \"0\"
        spec:
          containers:
            - name: kafka
              image: wurstmeister/kafka
              ports:
                - containerPort: 9092
              env:
                - name: KAFKA_ADVERTISED_PORT
                  value: \"9092\"
                - name: KAFKA_ADVERTISED_HOST_NAME
                  value: \"${kafka_host_name}\"
                - name: KAFKA_ZOOKEEPER_CONNECT
                  value: \"zoo1:2181\"
                - name: KAFKA_BROKER_ID
                  value: \"0\"
                - name: KAFKA_CREATE_TOPICS
                  value: \"glances:1:1\"" > deployments/kafka/tmp.yml)

    kubectl apply -f deployments/kafka/tmp.yml
    rm deployments/kafka/tmp.yml
fi

if [[ $no_spark -eq 0 ]] ; then
    echo "# -------- Skipped Spark deployment -------- #"
else
    echo "# -------- Déploiement de Spark -------- #"
    echo "Little sleep : Waiting for a ready tiller pod..."
    tiller_not_ready=true
    while $tiller_not_ready ; do
        tiller_etat=$(kubectl -n kube-system get po | grep tiller | awk '{print $2}')
        if [[ "$tiller_etat" -eq '1/1' ]] ; then
            tiller_not_ready=false
	else
            sleep 5
        fi
    done
    helm install --name my-spark stable/spark
fi

if [[ $no_cassandra -eq 0 ]] ; then
    echo "# -------- Skipped Cassandra's deployment -------- #"
else
    echo "# -------- Déploiement de Cassandra -------- #"
    helm repo add bitnami https://charts.bitnami.com
    helm repo udpate
    helm install --set service.type=LoadBalancer --name cassandra bitnami/cassandra
fi

echo "# -------- INFORMATIONS IMPORTANTES CI-DESSOUS -------- #"
echo "# -------- Infos sur le cluster... -------- #"
kubectl cluster-info

echo "# -------- Mot de passe Administrateur -------- #"
kops get secrets kube --type secret -oplaintext > tmp.txt
admin_mdp=$(echo "$(cat tmp.txt)")
rm tmp.txt

echo "# -------- Adresse du Dashboard.... -------- #"
echo "https://admin:${admin_mdp}@${cluster_url}/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/"

echo "# -------- Token du Service Account -------- #"
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}') | grep token: | awk '{print $2}'

echo "# -------- Accéder à Grafana... -------- #"
echo "Le lien sera disponible dans quelques instants..."
echo "Identifiants : admin/admin"
export grafana_url=$(kubectl get service grafana -n monitoring --output jsonpath='{.status.loadBalancer.ingress[0].hostname}')
echo "http://${grafana_url}"
