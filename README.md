# Kubernetes

La documentation pour déployer le cluster Kubernetes se trouve dans le dossier `kops`.

# Application de démonstration

Les instructions pour mettre en place l'application de démonstration se trouvent dans le dossier `zeppelin`.