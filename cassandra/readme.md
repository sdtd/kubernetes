Pour installer cassandra nous utilisons le repo de bitnami

```console
helm repo add bitnami https://charts.bitnami.com
helm repo udpate
helm install --set service.type=LoadBalancer --name bitcass bitnami/cassandra
```

Pour push les données directement de glances au cassandra en ligne

```console
pip install git+https://github.com/EmilienMottet/glances@add-auth-provider-cassandra --user
```

récupérer les credentials de votre cassandra
```console
# user
# cassandra
# password
kubectl get secret --namespace default bitcass-cassandra -o jsonpath="{.data.cassandra-password}" | base64 --decode
# url
kubectl get svc --namespace default bitcass-cassandra --template "{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}"
```

créer un fichier conf de cette forme
```ini
[cassandra]
host=af13d9fbc1bfe11e996dd06bea6d16b2-55904757.eu-west-3.elb.amazonaws.com
port=9042
protocol_version=3
keyspace=glances
replication_factor=2
table=localhost
username=cassandra
password=password
```

et lancer glances
```console
glances -C conf.yml --export cassandra
```


## Stress test cassandra

pour faire un stress test de cassandra

```
kubectl exec -it bitcass-cassandra-0 -- /opt/bitnami/cassandra/tools/bin/cassandra-stress write n=1000000 -mode native cql3 user=cassandra password=nFrt94azjr
kubectl exec -it bitcass-cassandra-0 -- /opt/bitnami/cassandra/tools/bin/cassandra-stress read n=1000000 -mode native cql3 user=cassandra password=nFrt94azjr
```
